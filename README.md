# CRUD User API Rest

This is a small test project designed to build a crud user operations rest api, hosted at AWS and using serverless to deploy it


## Installation 🔧
How to install NPM:
https://www.npmjs.com/get-npm

How to install AWS Cli:
https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html

How to install Serverless:
https://www.serverless.com/framework/docs/providers/aws/guide/installation/


## Deploy 📦

Once configured the AWS account in the developer machine, type * sls deploy * to deploy the system

## Flowchart

The main service flowchart can be seen in this image:
https://imgur.com/HISgkDO

## Test

You can test easy the API with the Postman integration of Swagger file at:
https://documenter.getpostman.com/view/331699/TW6zHnR1


## Author ✒️

* **César García**

## License 📄

This project is under MIT license