const userCommand = require("./controllers/command");

module.exports.execute = async (event) => {
  let bodyResponse;
  try {
    const queryParams = {};
    const response = await userCommand.query(queryParams);
    if (response.errorType) {
      throw Error(response.errorMessage);
    }
    bodyResponse = response;
  } catch (error) {
    return new BadResponse("Error in read message", error).getResponse();
  }

  return {
    statusCode: 200,
    body: JSON.stringify(bodyResponse),
  };
};
