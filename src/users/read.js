"use strict";
const uuidValidate = require("../utils/validators/uuid");
const BadResponse = require("../utils/responses/badRequest");
const userCommand = require("./controllers/command");

module.exports.execute = async (event) => {
  let uuid;
  if (event.pathParameters && event.pathParameters.uuid) {
    uuid = event.pathParameters.uuid;
  } else {
    return new BadResponse("No uuid specified").getResponse();
  }

  let bodyResponse;
  try {
    uuidValidate(uuid);

    const readParams = {
      uuid: uuid,
    };
    const response = await userCommand.read(readParams);
    if (response.errorType) {
      throw Error(response.errorMessage);
    }
    bodyResponse = response;
  } catch (error) {
    return new BadResponse("Error in read message", error).getResponse();
  }

  return {
    statusCode: 200,
    body: JSON.stringify(bodyResponse),
  };
};
