const uuidValidate = require("../../utils/validators/uuid");
const nameValidate = require("../../utils/validators/name");
const emailValidate = require("../../utils/validators/email");

module.exports = class User {
  constructor(uuid, name, email) {
    this.uuid = uuid;
    this.name = name;
    this.email = email;
  }

  validate() {
    uuidValidate(this.uuid);
    nameValidate(this.name);
    emailValidate(this.email);
  }

  format() {
    return {
      uuid: this.uuid,
      name: this.name,
      email: this.email,
    };
  }
};
