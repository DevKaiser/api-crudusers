"use strict";
const BadResponse = require("../utils/responses/badRequest");
const userCommand = require("./controllers/command");

module.exports.execute = async (event) => {
  let body;
  try {
    body = JSON.parse(event.body);
  } catch (err) {
    return new BadResponse("Not valid json in body", err);
  }

  let bodyResponse;
  try {
    const createParams = {
      name: body.name,
      email: body.email,
    };
    const response = await userCommand.create(createParams);
    if (response.errorType) {
      throw Error(response.errorMessage);
    }
    bodyResponse = response;
  } catch (error) {
    return new BadResponse("Error in create message", error).getResponse();
  }

  return {
    statusCode: 200,
    body: JSON.stringify(bodyResponse),
  };
};
