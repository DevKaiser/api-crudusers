const Message = require("./message").Message;
const MessageType = require("./message").MessageType;
const lambdaInvoke = require("../../utils/lambdaInvoke");

async function queryUsers(requestParams) {
  const message = new Message(MessageType.QUERY, requestParams);
  return await command(message);
}

async function readUser(requestParams) {
  const message = new Message(MessageType.READ, requestParams);
  return await command(message);
}

async function createUser(requestParams) {
  const message = new Message(MessageType.CREATE, requestParams);
  return await command(message);
}

async function updateUser(requestParams) {
  const message = new Message(MessageType.UPDATE, requestParams);
  return await command(message);
}

async function deleteUser(requestParams) {
  const message = new Message(MessageType.DELETE, requestParams);
  return await command(message);
}

async function command(message) {
  const functionName = "crudusers-dev-dynamodb-controller";
  const response = await lambdaInvoke(message, functionName);
  return response;
}

module.exports = {
  query: queryUsers,
  read: readUser,
  create: createUser,
  update: updateUser,
  delete: deleteUser,
};
