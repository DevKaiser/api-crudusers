const MessageType = {
  QUERY: 0,
  CREATE: 1,
  READ: 2,
  UPDATE: 3,
  DELETE: 4,
};

const MessageTypes = [
  MessageType.QUERY,
  MessageType.CREATE,
  MessageType.READ,
  MessageType.UPDATE,
  MessageType.DELETE,
];

class Message {
  constructor(type, request) {
    if (!MessageTypes.includes(type)) {
      throw Error("Invalid MessageType");
    }
    this.type = type;
    this.request = request;
  }
}

module.exports = {
  MessageType: MessageType,
  Message,
};
