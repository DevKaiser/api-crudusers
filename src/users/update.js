"use strict";
const BadResponse = require("../utils/responses/badRequest");
const userCommand = require("./controllers/command");

module.exports.execute = async (event) => {
  let uuid;
  if (event.pathParameters && event.pathParameters.uuid) {
    uuid = event.pathParameters.uuid;
  } else {
    return new BadResponse("No uuid specified").getResponse();
  }

  let body;
  try {
    body = JSON.parse(event.body);
  } catch (err) {
    return new BadResponse("Not valid json in body", err);
  }

  let bodyResponse;
  try {
    const updateParams = {
      uuid: uuid,
      name: body.name,
      email: body.email,
    };
    const response = await userCommand.update(updateParams);
    if (response.errorType) {
      throw Error(response.errorMessage);
    }
    bodyResponse = response;
  } catch (error) {
    return new BadResponse("Error in update message", error).getResponse();
  }

  return {
    statusCode: 200,
    body: JSON.stringify(bodyResponse),
  };
};
