const AWS = require("aws-sdk");
const Uuid = require("uuid");
const User = require("../users/models/user");
const MessageType = require("../users/controllers/message").MessageType;
const OkResponse = require("../utils/responses/okRequest");

AWS.config.update({ region: "eu-west-1" });
const dynamodb = new AWS.DynamoDB();

module.exports.command = async (event) => {
  console.log("Dynamodb command lambda starts");
  console.log("Events: " + JSON.stringify(event));

  let output;
  switch (event.type) {
    case MessageType.QUERY:
      output = await dbScan(event.request);
      break;
    case MessageType.CREATE:
      output = await dbCreate(event.request);
      break;
    case MessageType.READ:
      output = await dbRead(event.request);
      break;
    case MessageType.UPDATE:
      output = await dbUpdate(event.request);
      break;
    case MessageType.DELETE:
      output = await dbDelete(event.request);
      break;
  }

  return output;
};

async function dbScan(request) {
  const requestParams = {
    TableName: "usersTable",
  };

  let users = [];
  const response = await dynamodb.scan(requestParams).promise();
  if (response && response.Items) {
    for (const item of response.Items) {
      const user = new User(item.uuid.S, item.name.S, item.email.S);
      user.validate(); // Not really needed

      users.push(user.format());
    }
  }
  return {
    total_users: response.Count,
    listed_users: response.ScannedCount,
    users: users,
  };
}

async function dbCreate(request) {
  const uuid = Uuid.v4(); // new Uuid
  const user = new User(uuid, request.name, request.email);
  user.validate();

  const requestParams = {
    TableName: "usersTable",
    Item: {
      uuid: { S: uuid },
      name: { S: user.name },
      email: { S: user.email },
    },
  };

  await dynamodb.putItem(requestParams).promise();
  output = user.format();

  return output;
}

async function dbRead(request) {
  const requestParams = {
    TableName: "usersTable",
    Key: {
      uuid: { S: request.uuid },
    },
  };

  let output;
  const response = await dynamodb.getItem(requestParams).promise();
  if (response && response.Item) {
    const user = new User(
      response.Item.uuid.S,
      response.Item.name.S,
      response.Item.email.S
    );
    user.validate(); // Not really needed

    output = user.format();
  } else {
    throw Error("No user uuid found");
  }

  return output;
}

async function dbUpdate(request) {
  const user = new User(request.uuid, request.name, request.email);
  user.validate();

  const requestParams = {
    TableName: "usersTable",
    Key: {
      uuid: { S: user.uuid },
    },
    UpdateExpression: "set #name = :name, email = :email, isActive = :isActive",
    ConditionExpression: "attribute_exists(#uuid)",
    ExpressionAttributeValues: {
      ":name": { S: user.name },
      ":email": { S: user.email },
      ":isActive": { BOOL: false },
    },
    ExpressionAttributeNames: {
      "#uuid": "uuid", // uuid and name are reserved keywords in updateItem, so we have to give them alias
      "#name": "name",
    },
    ReturnValues: "ALL_NEW",
  };

  let output;
  const response = await dynamodb.updateItem(requestParams).promise();
  if (response && response.Attributes) {
    const attr = response.Attributes;
    const updatedUser = new User(attr.uuid.S, attr.name.S, attr.email.S);
    updatedUser.validate(); // Not really needed
    output = updatedUser.format();
  }

  return output;
}

async function dbDelete(request) {
  const requestParams = {
    TableName: "usersTable",
    Key: {
      uuid: { S: request.uuid },
    },
  };

  const response = await dynamodb.deleteItem(requestParams).promise();
  output = new OkResponse().getBody();

  return output;
}
