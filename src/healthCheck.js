"use strict";

module.exports.execute = async (event) => {
  return {
    statusCode: 200,
    body: JSON.stringify({
      message: "HealthCheck is working",
    }),
  };
};
