"use strict";

const mochaPlugin = require("serverless-mocha-plugin");
const dirtyChai = require("dirty-chai");
mochaPlugin.chai.use(dirtyChai);
const expect = mochaPlugin.chai.expect;
const AWS = require("aws-sdk");
let wrapped = mochaPlugin.getWrapper(
  "execute",
  "../../../src/users/create.js",
  "execute"
);

AWS.config.update({
  region: "eu-west-1",
});

describe("User Create", () => {
  before((done) => {
    done();
  });

  it("Create user with ok params", () => {
    return wrapped
      .run({
        body: JSON.stringify({
          name: "Paul Smith",
          email: "smith@hotmai.coml",
        }),
      })
      .then((response) => {
        console.log(response);
        expect(response.statusCode).to.be.equal(200);
      });
  });

  it("Create user with invalid email", () => {
    return wrapped
      .run({
        body: JSON.stringify({
          name: "Paul Smith",
          email: "smith@hotmail",
        }),
      })
      .then((response) => {
        console.log(response);
        expect(response.statusCode).to.be.equal(400);
      });
  });
});
