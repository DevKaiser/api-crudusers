"use strict";

const mochaPlugin = require("serverless-mocha-plugin");
const dirtyChai = require("dirty-chai");
mochaPlugin.chai.use(dirtyChai);
const expect = mochaPlugin.chai.expect;
const AWS = require("aws-sdk");
let wrapped = mochaPlugin.getWrapper(
  "execute",
  "../../../src/users/read.js",
  "execute"
);

AWS.config.update({
  region: "eu-west-1",
});

describe("User Read", () => {
  before((done) => {
    done();
  });

  it("Read user with ok uuid", () => {
    return wrapped
      .run({
        pathParameters: {
          uuid: "04b818b9-1837-4277-ba18-f8e9f1d19a7c",
        },
      })
      .then((response) => {
        console.log(response);
        expect(response.statusCode).to.be.equal(200);
      });
  });

  it("Read user with invalid uuid", () => {
    return wrapped
      .run({
        pathParameters: {
          uuid: "04b818b9-1837-4277-ba18-f8e9f1d",
        },
      })
      .then((response) => {
        console.log(response);
        expect(response.statusCode).to.be.equal(400);
      });
  });
});
