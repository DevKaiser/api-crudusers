"use strict";

const mochaPlugin = require("serverless-mocha-plugin");
const dirtyChai = require("dirty-chai");
mochaPlugin.chai.use(dirtyChai);
const expect = mochaPlugin.chai.expect;
const AWS = require("aws-sdk");
let wrapped = mochaPlugin.getWrapper(
  "execute",
  "../../../src/users/query.js",
  "execute"
);

AWS.config.update({
  region: "eu-west-1",
});

describe("Users Query", () => {
  before((done) => {
    done();
  });

  it("Query users", () => {
    return wrapped.run({}).then((response) => {
      console.log(response);
      expect(response.statusCode).to.be.equal(200);
    });
  });
});
