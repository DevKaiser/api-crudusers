"use strict";

const mochaPlugin = require("serverless-mocha-plugin");
const dirtyChai = require("dirty-chai");
mochaPlugin.chai.use(dirtyChai);
const expect = mochaPlugin.chai.expect;
const AWS = require("aws-sdk");
let wrapped = mochaPlugin.getWrapper(
  "execute",
  "../../../src/users/update.js",
  "execute"
);

AWS.config.update({
  region: "eu-west-1",
});

describe("User Update", () => {
  before((done) => {
    done();
  });

  it("Update user with ok params", () => {
    return wrapped
      .run({
        pathParameters: {
          uuid: "d329d220-3434-48cd-a928-3c03a05c2210",
        },
        body: JSON.stringify({
          name: "Paul Smith",
          email: "smith@hotmai.com",
        }),
      })
      .then((response) => {
        console.log(response);
        expect(response.statusCode).to.be.equal(200);
      });
  });

  it("Update user with invalid email", () => {
    return wrapped
      .run({
        pathParameters: {
          uuid: "d329d220-3434-48cd-a928-3c03a05c2210",
        },
        body: JSON.stringify({
          name: "Paul Smith",
          email: "smith@hotmail",
        }),
      })
      .then((response) => {
        console.log(response);
        expect(response.statusCode).to.be.equal(400);
      });
  });
});
