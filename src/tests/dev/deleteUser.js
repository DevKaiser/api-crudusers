"use strict";

const mochaPlugin = require("serverless-mocha-plugin");
const dirtyChai = require("dirty-chai");
mochaPlugin.chai.use(dirtyChai);
const expect = mochaPlugin.chai.expect;
const AWS = require("aws-sdk");
let wrapped = mochaPlugin.getWrapper(
  "execute",
  "../../../src/users/delete.js",
  "execute"
);

AWS.config.update({
  region: "eu-west-1",
});

describe("User Delete", () => {
  before((done) => {
    done();
  });

  it("Delete user with ok params", () => {
    return wrapped
      .run({
        pathParameters: {
          uuid: "8cee80be-2ad2-4418-9555-89021b09b4d3",
        },
      })
      .then((response) => {
        console.log(response);
        expect(response.statusCode).to.be.equal(200);
      });
  });

  it("Delete user with invalid uuid", () => {
    return wrapped
      .run({
        pathParameters: {
          uuid: "8cee80be-2ad2-4418-9555-89021b0",
        },
      })
      .then((response) => {
        console.log(response);
        expect(response.statusCode).to.be.equal(400);
      });
  });
});
