var AWS = require("aws-sdk");
AWS.config.region = "eu-west-1";
var lambda = new AWS.Lambda();

module.exports = async function (payload, functionName) {
  const params = {
    FunctionName: functionName,
    InvocationType: "RequestResponse",
    LogType: "Tail",
    Payload: JSON.stringify(payload),
  };

  const response = await lambda.invoke(params).promise();
  if (!response.Payload) {
    throw Error("No payload found on lambda call");
  }
  return JSON.parse(response.Payload);
};
