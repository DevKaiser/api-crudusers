module.exports = function validate(stringValue, stringMaxLenght) {
  if (stringValue.length > stringMaxLenght) {
    throw new Error("String value exceeded max value " + stringMaxLenght);
  }
};
