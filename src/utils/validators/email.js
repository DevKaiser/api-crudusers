const stringMaxLengthValidate = require("./stringMaxLength");

module.exports = function validate(email) {
  stringMaxLengthValidate(email, 255);
  const regExpr = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (!email.match(regExpr)) {
    throw new Error(`Email '${email}' value is not a valid email`);
  }
};
