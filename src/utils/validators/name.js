const stringMaxLenghtValidate = require("./stringMaxLength");

module.exports = function validate(name) {
  stringMaxLenghtValidate(name, 255);
  // TODO: Further check of non-UTF8 characters
};
