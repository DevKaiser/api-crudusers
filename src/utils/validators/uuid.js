const Uuid = require("uuid");

module.exports = function validate(uuid) {
  try {
    const uuidValidate = Uuid.validate(uuid);
    const uuidVersion = Uuid.version(uuid);
    if (!uuidValidate || uuidVersion != 4) {
      throw new Error("Error in validate uuid: " + uuid);
    }
  } catch (err) {
    throw new Error("Error in validate uuid: " + uuid);
  }
};
