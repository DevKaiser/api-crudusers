module.exports = class Response {
  constructor(statusCode, message, error = undefined) {
    this.statusCode = statusCode;
    this.message = message;
    this.error = error;
  }

  getResponse() {
    return {
      statusCode: this.statusCode,
      body: JSON.stringify({
        message: this.message,
        error: this.error ? this.error.message : undefined,
      }),
    };
  }

  getBody() {
    return {
      message: this.message,
      error: this.error ? this.error.message : undefined,
    };
  }
};
