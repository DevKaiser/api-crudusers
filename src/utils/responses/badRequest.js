const Response = require("./response");

module.exports = class BadRequestResponse extends Response {
  constructor(message, error = undefined) {
    super(400, message, error);
  }
};
