const Response = require("./response");

module.exports = class OkRequestResponse extends Response {
  constructor() {
    super(200, "ok");
  }
};
